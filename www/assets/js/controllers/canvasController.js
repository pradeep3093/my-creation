

app.controller('canvasController', function ($scope, Fabric, FabricConstants, Keypress, $ionicActionSheet, $timeout, $log) {

    $scope.fabric = {};
    $scope.FabricConstants = FabricConstants;

    //
    // Creating Canvas Objects
    // ================================================================
    $scope.addShape = function (path) {
        $scope.fabric.addShape('http://fabricjs.com/assets/15.svg');
    };

    $scope.addImage = function (imagePath) {
        $scope.fabric.addImage(imagePath);
    };

    $scope.addImageUpload = function (data) {
        var obj = angular.fromJson(data);
        $scope.addImage(obj.filename);
    };

    $scope.saveFile = function (fabric) {
        var json_data = fabric.getJSON();
        console.log(json_data);
    };

    $scope.loadFile = function (jsondata) {
        $scope.fabric.loadJSON(jsondata);
    }

    //
    // Editing Canvas Size
    // ================================================================
    $scope.selectCanvas = function () {
        $scope.canvasCopy = {
            width: $scope.fabric.canvasOriginalWidth,
            height: $scope.fabric.canvasOriginalHeight
        };
    };

    $scope.setCanvasSize = function () {
        $scope.fabric.setCanvasSize($scope.canvasCopy.width, $scope.canvasCopy.height);
        $scope.fabric.setDirty(true);
        delete $scope.canvasCopy;
    };

    //
    // Init
    // ================================================================
    $scope.init = function () {
        $scope.fabric = new Fabric({
            JSONExportProperties: FabricConstants.JSONExportProperties,
            textDefaults: FabricConstants.textDefaults,
            shapeDefaults: FabricConstants.shapeDefaults,
            json: {}
        });
    };

    $scope.$on('canvas:created', $scope.init);

    Keypress.onSave(function () {
        $scope.updatePage();
    });
    $scope.data = {};
    var imageEncoded;

    /**
     * adding the new photo
     * title and image are mandatory, called from scope
     * 
     * @returns {Boolean}
     */
    $scope.newPost = function () {

        $scope.data.file = dataURItoBlob($scope.picture);
        $log.log("file", $scope.data.file);

        var fileData = new FormData();
        fileData.append('title', $scope.data.title);
        fileData.append('file', $scope.data.file, "photo.jpeg");

        $log.log(fileData);


    };


    /**
     * action sheet options, called from  scope
     */

    $scope.addImageActionsheet = function () {

        $ionicActionSheet.show({
            titleText: 'Open',
            buttons: [
                {text: '<i class="icon ion-camera"></i> Camera'},
                {text: '<i class="icon ion-folder"></i> Gallery'}
            ],
            cancelText: 'Cancel',
            cancel: function () {
                $log.log('Cancel');
            },
            buttonClicked: function (index) {
                if (index === 0) {
                    takePicture();
                    return true;
                }
                if (index === 1) {
                    getPicture();
                    return true;
                }
            }

        });
    };

    var takePicture = function () {

        var options = {
            quality: 100,
            targetWidth: 1920,
            targetHeight: 1080,
            sourceType: 1,
//            cameraDirection: 'PRIMARY',
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL
        };

        navigator.camera.getPicture(onSuccess, onFail, options);

        function onSuccess(imageData) {

            var picture;
            var testImage = new Image();

            $timeout(function () {
                picture = "data:image/jpeg;base64," + imageData;
                testImage.src = picture;

            });



            testImage.onload = function () {
                $log.log(testImage.width + ", " + testImage.height);
                $log.log("natural", testImage.naturalWidth + ", " + testImage.naturalHeight);

                if (testImage.width >= 580 && testImage.height >= 580) {

                    imageEncoded = imageData;
                    $scope.addImage(testImage.src);

                } else {


                }
            };


        }

        function onFail(message) {

        }

    };

    /**
     * Convert the image from data uri to blob, so that it can appended for api
     * call
     * 
     * @param {type} dataURI
     * 
     * @returns {Blob}
     * 
     */
    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    }

    var getPicture = function () {

        var options = {
            quality: 100,
            targetWidth: 1920,
            targetHeight: 1080,
            sourceType: 0,
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL
        };



        navigator.camera.getPicture(onSuccess, onFail, options);

        function onSuccess(imageData) {

            var picture;
            var testImage = new Image();

            $timeout(function () {
                picture = "data:image/jpeg;base64," + imageData;
                testImage.src = picture;

            });



            testImage.onload = function () {
                $log.log(testImage.width + ", " + testImage.height);
                $log.log("natural", testImage.naturalWidth + ", " + testImage.naturalHeight);
                ;
                if (testImage.width >= 580 && testImage.height >= 580) {

                    imageEncoded = imageData;
                    $scope.addImage(testImage.src);

                } else {


                }
            };


        }

        function onFail(message) {

        }

    };
    $scope.Download = function () {
        ionic.Platform.ready(function () {
            var url = "http://3.bp.blogspot.com/-XchURXRz-5c/U5ApPOrPM9I/AAAAAAAADoo/YZEj4qeSlqo/s1600/Final-Fantasy-XV-Noctis-Red-Eyes.png";
            var filename = url.split("/").pop();
            var targetPath = cordova.file.externalRootDirectory + 'Pictures/' + filename;

            $cordovaFileTransfer.download(url, targetPath, {}, true).then(function (result) {
                $scope.hasil = 'Save file on ' + targetPath + ' success!';
                $scope.mywallpaper = targetPath;
            }, function (error) {
                $scope.hasil = 'Error Download file';
            }, function (progress) {
                $scope.downloadProgress = (progress.loaded / progress.total) * 100;
            });
        });
    }
});